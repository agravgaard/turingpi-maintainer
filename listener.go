package main

import (
	"context"
	"flag"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"k8s.io/client-go/tools/leaderelection"
	"k8s.io/klog/v2"

	election "gitlab.com/agravgaard/turingpi-maintainer/pkg/election"
	nodehealth "gitlab.com/agravgaard/turingpi-maintainer/pkg/nodehealth"
)

func main() {
	flags := flag.CommandLine
	klog.InitFlags(flags)
	electionLock := election.ParseFlags(flags)

	go func() {
		http.Handle("/metrics", promhttp.Handler())
		http.ListenAndServe(":2112", nil)
	}()
	// use a Go context so we can tell the leaderelection code when we
	// want to step down
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// listen for interrupts or the Linux SIGTERM signal and cancel
	// our context, which the leader election code will observe and
	// step down
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-ch
		klog.Info("Received termination, signaling shutdown")
		cancel()
	}()

	lock := electionLock.GetLock()
	id := electionLock.GetID()
	tc := election.GetTimeConfig(60, 15, 5)

	callbacks := leaderelection.LeaderCallbacks{
		OnStartedLeading: func(ctx context.Context) {
			// we're notified when we start - this is where you would
			// usually put your code
			nodehealth.Run(ctx)
		},
		OnStoppedLeading: func() {
			// we can do cleanup here
			klog.Infof("leader lost: %s", id)
			os.Exit(0)
		},
		OnNewLeader: func(identity string) {
			// we're notified when new leader elected
			if identity == id {
				// I just got the lock
				return
			}
			klog.Infof("new leader elected: %s", identity)
		},
	}

	// start the leader election code loop
	leaderelection.RunOrDie(ctx, tc.GetConfig(lock, callbacks))
}
