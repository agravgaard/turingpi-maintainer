FROM gcr.io/distroless/static:nonroot AS final
ARG TARGETOS
ARG TARGETARCH

WORKDIR /

COPY ./listener-$TARGETOS-$TARGETARCH /listener

USER 69420:69420

ENTRYPOINT ["/listener", "-logtostderr=true", "-lease-lock-name=listener-lock"]
