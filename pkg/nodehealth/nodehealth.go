package nodehealth

import (
	"context"
	"fmt"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/klog/v2"

	i2c "gitlab.com/agravgaard/turingpi-maintainer/pkg/i2c"
)

var (
	nodeRebootCount = promauto.NewCounter(prometheus.CounterOpts{
		Name: "turingpimaintainer_node_reboot_count_total",
		Help: "The total number of node reboots",
	})
)

func nodeNumToName(node uint8) string {
	return fmt.Sprintf("worker%d", node)
}

func isReady(node *v1.Node) bool {
	for _, c := range node.Status.Conditions {
		switch ct := c.Type; ct {
		case v1.NodeReady:
			if c.Status != v1.ConditionTrue {
				return false
			}
		case "ReadonlyFilesystem":
			if c.Status == v1.ConditionTrue {
				return false
			}
		}
	}
	return true
}

func isNodeReady(node uint8, clientset kubernetes.Interface, ctx context.Context) bool {
	name := nodeNumToName(node)
	nodedesc, err := clientset.CoreV1().Nodes().Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		klog.Fatal(err)
	}

	return isReady(nodedesc)
}

func Run(ctx context.Context) {
	// complete your controller loop here
	klog.Info("Controller loop...")
	// Get client config
	config, err := rest.InClusterConfig()
	if err != nil {
		klog.Fatal(err)
	}
	client := kubernetes.NewForConfigOrDie(config)

	deadLimit := uint8(3)       // times to try before reboot
	interval := 5 * time.Minute // how often to try
	nodes := []uint8{1, 2, 3, 4, 5, 6, 7}
	deadCount := make([]uint8, len(nodes))
	for {
		for i, node := range nodes {
			if !isNodeReady(node, client, ctx) {
				deadCount[i]++
			}
			if deadCount[i] > deadLimit {
				i2c.Init()
				i2c.RebootNode("1", node)
				deadCount[i] = 0
				nodeRebootCount.Inc()
			}
		}
		time.Sleep(interval)
	}
}
