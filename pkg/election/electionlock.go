package election

import (
	"flag"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	clientset "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/leaderelection/resourcelock"
	"k8s.io/klog/v2"
)

type ElectionLock struct {
	leaseLockName      string
	leaseLockNamespace string
	id                 string
}

func ParseFlags(flags *flag.FlagSet) ElectionLock {
	var electionLock ElectionLock

	flags.StringVar(&electionLock.id, "id", "", "the holder identity name")
	flags.StringVar(&electionLock.leaseLockName, "lease-lock-name", "", "the lease lock resource name")
	flags.StringVar(&electionLock.leaseLockNamespace, "lease-lock-namespace", "", "the lease lock resource namespace")

	// Parse all defined flags
	flag.Parse()

	if electionLock.id == "" {
		klog.Fatal("no id supplied! (missing id flag).")
	}
	if electionLock.leaseLockName == "" {
		klog.Fatal("unable to get lease lock resource name (missing lease-lock-name flag).")
	}
	if electionLock.leaseLockNamespace == "" {
		klog.Fatal("unable to get lease lock resource namespace (missing lease-lock-namespace flag).")
	}

	return electionLock
}

func (electionLock *ElectionLock) GetID() string {
	return electionLock.id
}

func (electionLock *ElectionLock) GetLock() *resourcelock.LeaseLock {

	// leader election uses the Kubernetes API by writing to a
	// lock object, which can be a LeaseLock object (preferred),
	// a ConfigMap, or an Endpoints (deprecated) object.
	// Conflicting writes are detected and each client handles those actions
	// independently.
	config, err := rest.InClusterConfig()
	if err != nil {
		klog.Fatal(err)
	}
	client := clientset.NewForConfigOrDie(config)

	// we use the Lease lock type since edits to Leases are less common
	// and fewer objects in the cluster watch "all Leases".
	return &resourcelock.LeaseLock{
		LeaseMeta: metav1.ObjectMeta{
			Name:      electionLock.leaseLockName,
			Namespace: electionLock.leaseLockNamespace,
		},
		Client: client.CoordinationV1(),
		LockConfig: resourcelock.ResourceLockConfig{
			Identity: electionLock.id,
		},
	}
}
