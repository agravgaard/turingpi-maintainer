package election

import (
	"time"

	"k8s.io/client-go/tools/leaderelection"
	"k8s.io/client-go/tools/leaderelection/resourcelock"
)

type TimeConfig struct {
	Lease         time.Duration
	RenewDeadline time.Duration
	RetryPeriod   time.Duration
}

func GetTimeConfig(lease, renewal, retry time.Duration) TimeConfig {
	return TimeConfig{
		Lease:         lease * time.Second,
		RenewDeadline: renewal * time.Second,
		RetryPeriod:   retry * time.Second,
	}
}

func (tc *TimeConfig) GetConfig(lock resourcelock.Interface, callbacks leaderelection.LeaderCallbacks) leaderelection.LeaderElectionConfig {
	return leaderelection.LeaderElectionConfig{
		Lock: lock,
		// IMPORTANT: you MUST ensure that any code you have that
		// is protected by the lease must terminate **before**
		// you call cancel. Otherwise, you could have a background
		// loop still running and another process could
		// get elected before your background loop finished, violating
		// the stated goal of the lease.
		ReleaseOnCancel: true,
		LeaseDuration:   tc.Lease,
		RenewDeadline:   tc.RenewDeadline,
		RetryPeriod:     tc.RetryPeriod,
		Callbacks:       callbacks,
	}
}
