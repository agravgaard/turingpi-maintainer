package i2c

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"periph.io/x/conn/v3/i2c"
	"periph.io/x/conn/v3/physic"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestGetMask(t *testing.T) {
	out := getMask(3)
	assert.Equal(t, byte(0x08), out)
}

func TestWriteReg(t *testing.T) {
	b := &fakeBus{r: []byte{0x01}}
	d := i2c.Dev{Bus: b, Addr: 0x57}
	writeReg(&d, 0xf2, 0x04)
	assert.Equal(t, byte(0x04), b.w[1])
}

func TestReadReg(t *testing.T) {
	b := &fakeBus{r: []byte{0x01}}
	d := i2c.Dev{Bus: b, Addr: 0x57}
	out := readReg(&d, 0xf2)
	assert.Equal(t, byte(0x01), out)
}

func TestBinaryMask(t *testing.T) {
	tests := []struct {
		value    byte
		oldvalue byte
		mask     byte
		expected byte
	}{
		{0xff, 0x18, 0x02, 0x1a},
		{0x00, 0x18, 0x02, 0x18},
		{0x00, 0x18, 0x08, 0x10},
	}

	for _, tt := range tests {
		out := binaryMask(tt.value, tt.oldvalue, tt.mask)
		assert.Equal(t, tt.expected, out)
	}
}

type fakeBus struct {
	freq physic.Frequency
	err  error
	addr uint16
	w, r []byte
}

func (f *fakeBus) Close() error {
	return nil
}

func (f *fakeBus) String() string {
	return "fake"
}

func (f *fakeBus) Tx(addr uint16, w, r []byte) error {
	f.addr = addr
	f.w = append(f.w, w...)
	copy(r, f.r)
	f.r = f.r[len(r):]
	return f.err
}

func (f *fakeBus) SetSpeed(freq physic.Frequency) error {
	f.freq = freq
	return f.err
}
