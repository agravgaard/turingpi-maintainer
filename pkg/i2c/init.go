package i2c

import (
	"k8s.io/klog/v2"
	"periph.io/x/host/v3"
)

func Init() {
	// Make sure periph is initialized.
	state, err := host.Init()
	if err != nil {
		klog.Fatalf("failed to initialize periph: %v", err)
	}

	// Prints the loaded driver.
	klog.Infof("Using drivers:\n")
	for _, driver := range state.Loaded {
		klog.Infof("- %s\n", driver)
	}

	// Prints the driver that were skipped as irrelevant on the platform.
	klog.Infof("Drivers skipped:\n")
	for _, failure := range state.Skipped {
		klog.Infof("- %s: %s\n", failure.D, failure.Err)
	}

	// Having drivers failing to load may not require process termination. It
	// is possible to continue to run in partial failure mode.
	klog.Infof("Drivers failed to load:\n")
	for _, failure := range state.Failed {
		klog.Infof("- %s: %v\n", failure.D, failure.Err)
	}
}
