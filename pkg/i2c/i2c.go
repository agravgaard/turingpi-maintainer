package i2c

import (
	"time"

	"k8s.io/klog/v2"
	"periph.io/x/conn/v3/i2c"
	"periph.io/x/conn/v3/i2c/i2creg"
)

// We want to do the equivalent of:
// i2cset -m $line -y 1 0x57 0xf2 0x00
// to shutdown, followed by, after a short sleep:
// i2cset -m $line -y 1 0x57 0xf2 0xff
// to boot again
// Where $line is mask for the node target
// From man-pages:
// i2cset [-f] [-y] [-m mask] [-r] [-a] i2cbus chip-address data-address [value] ...  [mode]
func getMask(node uint8) byte {
	switch node {
	case 1:
		return 0x02
	case 2:
		return 0x04
	case 3:
		return 0x08
	case 4:
		return 0x10
	case 5:
		return 0x80
	case 6:
		return 0x40
	case 7:
		return 0x20
	default:
		klog.Fatal("Node doesn't exist")
	}
	return 0x00
}

func writeReg(d *i2c.Dev, reg, value byte) {
	write := make([]byte, 2)
	write[0] = reg
	write[1] = value
	if _, err := d.Write(write); err != nil {
		klog.Fatal(err)
	}
}

func readReg(d *i2c.Dev, reg byte) byte {
	read := make([]byte, 1)
	write := make([]byte, 1)
	write[0] = reg
	if err := d.Tx(write, read); err != nil {
		klog.Fatal(err)
	}
	klog.Infof("%x\n", read[0])
	return read[0]
}

func binaryMask(value, oldvalue, vmask byte) byte {
	return (value & vmask) | (oldvalue & ^vmask)
}

func RebootNode(busName string, node uint8) {
	// Use i2creg I²C port registry to find the first available I²C bus.
	bus, err := i2creg.Open(busName)
	if err != nil {
		klog.Fatal(err)
	}
	defer bus.Close()

	d := &i2c.Dev{Addr: 0x57, Bus: bus}
	reg := byte(0xf2) // 0xf2 = registry for on/off

	regValue := readReg(d, reg)
	vmask := getMask(node)

	value := binaryMask(0x00, regValue, vmask) // 0x00 = turn off
	klog.Infof("turning off node %d", node)
	klog.Infof(" - by assigning %x to register %x", value, reg)
	writeReg(d, reg, value)
	regValue = value // the register should now be assigned value

	waitTime := 2 * time.Second
	klog.Infof("waiting for %d seconds", waitTime)
	time.Sleep(waitTime)

	value = binaryMask(0xff, regValue, vmask) // 0xff = turn on
	klog.Infof("turning on node %d", node)
	klog.Infof(" - by assigning %x to register %x", value, reg)
	writeReg(d, reg, value)
	klog.Infof("waiting for %d seconds", waitTime)
	time.Sleep(waitTime)
	klog.Info("done.")
}
