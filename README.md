# Turing Pi Maintainer

## Build

First build the go binary for your arch, e.g.:
```
CGO_ENABLED=0 GOOS=linux GOARCH=arm64 \
  go build \
     -ldflags "-extldflags '-static'" \
     -o listener-$GOOS-$GOARCH
```

Build docker image (setup buildx and login to your docker registry first):
```
docker buildx build --platform linux/arm64 -t "mytag" --push .
```

## Deploy to Kubernetes

First create the namespace:
```
kubectl apply -f k8s/namespace.yaml
```
Apply kustomization and apply:
```
kustomize build | kubectl apply -f -
```

## Setup a rootless kubernetes test-environment with podman

Install and setup rootless Podman, [see the guide from the Arch Linux Wiki](https://wiki.archlinux.org/title/Podman#Rootless_Podman).

Setup KinD for rootless Podman, [as in the documentation](https://kind.sigs.k8s.io/docs/user/rootless/). See notes for MacOS below.

Setup KinD with the config provided:
```
KIND_EXPERIMENTAL_PROVIDER=podman kind create cluster --config ./kind-node.yaml
```
This should create a kubernetes cluster with 3 nodes, one control-plane and two workers.

### MacOS

On MacOS the above guide for KinD should be performed inside the podman machine, i.e. `podman machine ssh`.

[Check this gist for a complete guide](https://gist.github.com/cmoulliard/ed0f6d4dd431e53b917b836bae4d0501).

## Acknowledgement

Written with inspiration from:
 * [kubernetes/client-go/examples/leader-election](https://github.com/kubernetes/client-go/tree/master/examples/leader-election)
 * [arm-research/smarter/smarter-device-manager](https://gitlab.com/arm-research/smarter/smarter-device-manager)
 * [Hua Zhang's blog](https://yuezhizizhang.github.io/kubernetes/kubectl/client-go/2020/05/13/kubectl-client-go-part-1.html)
